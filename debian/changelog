gtkglextmm (1.2.0-9) UNRELEASED; urgency=low

  * Trim trailing whitespace.
  * Bump debhelper from deprecated 9 to 10.
  * Change priority extra to priority optional.
  * Drop unnecessary dependency on dh-autoreconf.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 27 Jun 2020 12:10:16 -0000

gtkglextmm (1.2.0-8) unstable; urgency=medium

  * d/control:
    - New maintainer, Closes: #674875
    - Update standards version to 3.9.8
    - Add packaging VCS urls
    - run 'dpkg fix dpkg-control'
  * d/rules:
    - Add hardening
    - Add CPPFLAGS -DGLIBMM_DISABLE_DEPRECATED Closes: #838808
  * d/libgtkglextmm-x11-1.2-doc.install: don't copy useless md5 files
  * d/
    - Add doc-base file
    - Add override for missing symbols file

 -- Gert Wollny <gw.fossdev@gmail.com>  Fri, 07 Oct 2016 10:23:33 +0200

gtkglextmm (1.2.0-7) unstable; urgency=medium

  * QA upload
  * Use dh-autoreconf to support new ports
    - drop fix_as_needed patch, we should no longer need to patch ltmain.sh
      now that we are copying in the one from libtool
    - add patch from upstream to set the correct ACLOCAL_AMFLAGS
    - add patch to fix FTBFS with modern glibmm, which has moved its
      Autoconf macros and other metadata
  * Rename library package to libgtkglextmm-x11-1.2-0v5 for libstdc++
    ABI transition
    - version the build-dependencies to ensure the g++-5-compiled versions
  * Set the Section of the new package to oldlibs since it is based
    on the deprecated Gtk 2

 -- Simon McVittie <smcv@debian.org>  Tue, 25 Aug 2015 09:44:58 +0100

gtkglextmm (1.2.0-6) unstable; urgency=low

  * QA upload.
  * debian/ cleanup:
    + use dh
    + bump debhelper compat
    + use source format 3.0 (quilt)
    + remove invalid VCS headers
    + add Multi-Arch support
    + add missing dependencies of -dev package (Closes: #594187)
  * Apply patch by Andres Mejia to fix autoconf warning.
    (Closes: #580829)

 -- Frank Lichtenheld <djpig@debian.org>  Fri, 17 May 2013 21:31:51 +0200

gtkglextmm (1.2.0-5) unstable; urgency=low

  * QA upload. (Closes: #631783)
  * Change maintainer to QA Group.
  * Fix FTBFS with newer GTK by switching #includes around
    so that the G_DISABLE_DEPRECATED trick in glibmm still
    works (Closes: #707356)

 -- Frank Lichtenheld <djpig@debian.org>  Thu, 16 May 2013 23:51:57 +0200

gtkglextmm (1.2.0-4.1) unstable; urgency=low

  * Non-maintainer upload.
  * Remove references to other libraries from dependency_libs field
    (Closes: #620633).
  * Define GdkSpanFunc (Closes: #621976).

 -- Luk Claes <luk@debian.org>  Mon, 27 Jun 2011 08:17:17 +0200

gtkglextmm (1.2.0-4) unstable; urgency=low

  * Switch libgtkglextmm-x11-1.2-doc to arch all. Closes: #517606.

 -- Bradley Smith <bradsmith@debian.org>  Sat, 28 Feb 2009 21:14:06 +0000

gtkglextmm (1.2.0-3) unstable; urgency=low

  * Update maintainer email address.
  * Update compat version to 7 and upgrade debhelper Build-Depends.
  * Add Vcs-* fields.
  * Convert copyright to machine readable format.

 -- Bradley Smith <bradsmith@debian.org>  Tue, 30 Dec 2008 21:53:43 +0000

gtkglextmm (1.2.0-2) unstable; urgency=low

  * Add missing dependency on libgtkglext1-dev. Closes: #493381.
  * Bump Standards-Version to 3.8.0 (No changes).

 -- Bradley Smith <brad@brad-smith.co.uk>  Sat, 02 Aug 2008 18:24:43 +0100

gtkglextmm (1.2.0-1) unstable; urgency=low

  * Initial release. Closes: #476293
  * Add fix_as-needed patch
   - Fix ordering for --as-needed in libtool

 -- Bradley Smith <brad@brad-smith.co.uk>  Sun, 11 May 2008 16:12:00 +0100
