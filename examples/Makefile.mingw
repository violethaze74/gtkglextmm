# -*- Makefile -*-
#
# Sample makefile for gtkglextmm example programs
#   Naofumi Yasufuku <naofumi@users.sourceforge.net>
#

SHELL = /bin/sh

INCLUDES := $(shell pkg-config --cflags gtkglextmm-1.0)
LIBS := $(shell pkg-config --libs gtkglextmm-1.0)
DEFINES = 

CC = gcc
CXX = g++
OPTIMIZE = -march=pentium -O2
DEBUG = -g -Wall
MS_BITFIELDS = -mms-bitfields
#MS_BITFIELDS = -fnative-struct
CFLAGS = $(OPTIMIZE) $(DEBUG) $(MS_BITFIELDS)
CXXFLAGS = $(OPTIMIZE) $(DEBUG) $(MS_BITFIELDS)
CPPFLAGS = $(INCLUDES) $(DEFINES)
LDFLAGS = 

COMPILE_C = $(CC) $(CFLAGS) $(CPPFLAGS)
COMPILE_CXX = $(CXX) $(CXXFLAGS) $(CPPFLAGS)
LINK = $(CXX) $(LDFLAGS)

EXEEXT = .exe

HEADERS = \
	trackball.h		\
	shapes.h		\
	logo-model.h		\
	logo.h

SOURCES_CXX = \
	simple.cc		\
	simple-mixed.cc		\
	pixmap.cc		\
	pixmap-mixed.cc		\
	share-lists.cc		\
	font.cc			\
	button.cc		\
	shapes.cc		\
	logo.cc			\
	gears.cc		\
	simple-darea.cc

SOURCES_C = \
	trackball.c		\
	logo-model.c

EXTRA_SOURCES = \
	logo-g.c		\
	logo-t.c		\
	logo-k.c

OBJECTS = $(SOURCES_CXX:.cc=.o) $(SOURCES_C:.c=.o)

PROGRAMS = \
	simple$(EXEEXT)			\
	simple-mixed$(EXEEXT)		\
	pixmap$(EXEEXT)			\
	pixmap-mixed$(EXEEXT)		\
	share-lists$(EXEEXT)		\
	font$(EXEEXT)			\
	button$(EXEEXT)			\
	shapes$(EXEEXT)			\
	logo$(EXEEXT)			\
	gears$(EXEEXT)			\
	simple-darea$(EXEEXT)

.PHONY: all clean clean-deps distclean

.SUFFIXES:
.SUFFIXES: .c .cc .o

all: $(PROGRAMS)

#
# Include dependencies
#

DEPS_MAGIC := $(shell if test ! -d .deps; then mkdir .deps; fi)
DEPS = \
	$(addprefix .deps/, $(SOURCES_CXX:.cc=.Po))	\
	$(addprefix .deps/, $(SOURCES_C:.c=.Po))
-include $(DEPS)

#
# Compile command
#

.c.o:
	$(COMPILE_C) -Wp,-MD,.deps/$(*F).TPo -c $<
	@-cp .deps/$(*F).TPo .deps/$(*F).Po; \
	tr ' ' '\012' < .deps/$(*F).TPo \
		| sed -e 's/^\\$$//' -e '/^$$/d' -e '/:$$/d' \
		| sed -e 's/$$/ :/' \
		>> .deps/$(*F).Po; \
	rm .deps/$(*F).TPo

.cc.o:
	$(COMPILE_CXX) -Wp,-MD,.deps/$(*F).TPo -c $<
	@-cp .deps/$(*F).TPo .deps/$(*F).Po; \
	tr ' ' '\012' < .deps/$(*F).TPo \
		| sed -e 's/^\\$$//' -e '/^$$/d' -e '/:$$/d' \
		| sed -e 's/$$/ :/' \
		>> .deps/$(*F).Po; \
	rm .deps/$(*F).TPo

#
# Building executables
#

simple$(EXEEXT): simple.o
	$(LINK) -o $@ $^ $(LIBS)

simple-mixed$(EXEEXT): simple-mixed.o
	$(LINK) -o $@ $^ $(LIBS)

pixmap$(EXEEXT): pixmap.o
	$(LINK) -o $@ $^ $(LIBS)

pixmap-mixed$(EXEEXT): pixmap-mixed.o
	$(LINK) -o $@ $^ $(LIBS)

share-lists$(EXEEXT): share-lists.o
	$(LINK) -o $@ $^ $(LIBS)

font$(EXEEXT): font.o
	$(LINK) -o $@ $^ $(LIBS)

button$(EXEEXT): button.o
	$(LINK) -o $@ $^ $(LIBS)

shapes$(EXEEXT): trackball.o shapes.o
	$(LINK) -o $@ $^ $(LIBS)

logo$(EXEEXT): trackball.o logo-model.o logo.o
	$(LINK) -o $@ $^ $(LIBS)

gears$(EXEEXT): gears.o
	$(LINK) -o $@ $^ $(LIBS)

simple-darea$(EXEEXT): simple-darea.o
	$(LINK) -o $@ $^ $(LIBS)

#
# Clean up
#

clean-deps:
	-rm -rf .deps

clean-obj: clean-deps
	-rm -f *.o

clean: clean-obj
	-rm -f $(PROGRAMS)

distclean: clean
	-rm -f *~
