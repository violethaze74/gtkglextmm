;; -*- scheme -*-
; object definitions ...
(define-object Config
  (in-module "Gdk")
  (parent "GObject")
  (c-name "GdkGLConfig")
  (gtype-id "GDK_TYPE_GL_CONFIG")
)

(define-object Context
  (in-module "Gdk")
  (parent "GObject")
  (c-name "GdkGLContext")
  (gtype-id "GDK_TYPE_GL_CONTEXT")
)

;(define-interface Drawable
;  (in-module "Gdk")
;  (c-name "GdkGLDrawable")
;  (gtype-id "GDK_TYPE_GL_DRAWABLE")
;)

(define-object Pixmap
  (in-module "Gdk")
  (parent "GdkDrawable")
  (c-name "GdkGLPixmap")
  (gtype-id "GDK_TYPE_GL_PIXMAP")
)

(define-object Window
  (in-module "Gdk")
  (parent "GdkDrawable")
  (c-name "GdkGLWindow")
  (gtype-id "GDK_TYPE_GL_WINDOW")
)

;; Enumerations and flags ...

(define-flags ConfigMode
  (in-module "Gdk")
  (c-name "GdkGLConfigMode")
  (gtype-id "GDK_TYPE_GL_CONFIG_MODE")
  (values
    '("rgb" "GDK_GL_MODE_RGB")
    '("rgba" "GDK_GL_MODE_RGBA")
    '("index" "GDK_GL_MODE_INDEX")
    '("single" "GDK_GL_MODE_SINGLE")
    '("double" "GDK_GL_MODE_DOUBLE")
    '("stereo" "GDK_GL_MODE_STEREO")
    '("alpha" "GDK_GL_MODE_ALPHA")
    '("depth" "GDK_GL_MODE_DEPTH")
    '("stencil" "GDK_GL_MODE_STENCIL")
    '("accum" "GDK_GL_MODE_ACCUM")
    '("multisample" "GDK_GL_MODE_MULTISAMPLE")
  )
)

(define-flags DebugFlag
  (in-module "Gdk")
  (c-name "GdkGLDebugFlag")
  (gtype-id "GDK_TYPE_GL_DEBUG_FLAG")
  (values
    '("misc" "GDK_GL_DEBUG_MISC")
    '("func" "GDK_GL_DEBUG_FUNC")
    '("impl" "GDK_GL_DEBUG_IMPL")
  )
)

(define-enum ConfigAttrib
  (in-module "Gdk")
  (c-name "GdkGLConfigAttrib")
  (gtype-id "GDK_TYPE_GL_CONFIG_ATTRIB")
  (values
    '("use-gl" "GDK_GL_USE_GL")
    '("buffer-size" "GDK_GL_BUFFER_SIZE")
    '("level" "GDK_GL_LEVEL")
    '("rgba" "GDK_GL_RGBA")
    '("doublebuffer" "GDK_GL_DOUBLEBUFFER")
    '("stereo" "GDK_GL_STEREO")
    '("aux-buffers" "GDK_GL_AUX_BUFFERS")
    '("red-size" "GDK_GL_RED_SIZE")
    '("green-size" "GDK_GL_GREEN_SIZE")
    '("blue-size" "GDK_GL_BLUE_SIZE")
    '("alpha-size" "GDK_GL_ALPHA_SIZE")
    '("depth-size" "GDK_GL_DEPTH_SIZE")
    '("stencil-size" "GDK_GL_STENCIL_SIZE")
    '("accum-red-size" "GDK_GL_ACCUM_RED_SIZE")
    '("accum-green-size" "GDK_GL_ACCUM_GREEN_SIZE")
    '("accum-blue-size" "GDK_GL_ACCUM_BLUE_SIZE")
    '("accum-alpha-size" "GDK_GL_ACCUM_ALPHA_SIZE")
    '("config-caveat" "GDK_GL_CONFIG_CAVEAT")
    '("x-visual-type" "GDK_GL_X_VISUAL_TYPE")
    '("transparent-type" "GDK_GL_TRANSPARENT_TYPE")
    '("transparent-index-value" "GDK_GL_TRANSPARENT_INDEX_VALUE")
    '("transparent-red-value" "GDK_GL_TRANSPARENT_RED_VALUE")
    '("transparent-green-value" "GDK_GL_TRANSPARENT_GREEN_VALUE")
    '("transparent-blue-value" "GDK_GL_TRANSPARENT_BLUE_VALUE")
    '("transparent-alpha-value" "GDK_GL_TRANSPARENT_ALPHA_VALUE")
    '("drawable-type" "GDK_GL_DRAWABLE_TYPE")
    '("render-type" "GDK_GL_RENDER_TYPE")
    '("x-renderable" "GDK_GL_X_RENDERABLE")
    '("fbconfig-id" "GDK_GL_FBCONFIG_ID")
    '("max-pbuffer-width" "GDK_GL_MAX_PBUFFER_WIDTH")
    '("max-pbuffer-height" "GDK_GL_MAX_PBUFFER_HEIGHT")
    '("max-pbuffer-pixels" "GDK_GL_MAX_PBUFFER_PIXELS")
    '("visual-id" "GDK_GL_VISUAL_ID")
    '("screen" "GDK_GL_SCREEN")
    '("sample-buffers" "GDK_GL_SAMPLE_BUFFERS")
    '("samples" "GDK_GL_SAMPLES")
  )
)

(define-enum ConfigCaveat
  (in-module "Gdk")
  (c-name "GdkGLConfigCaveat")
  (gtype-id "GDK_TYPE_GL_CONFIG_CAVEAT")
  (values
    '("config-caveat-dont-care" "GDK_GL_CONFIG_CAVEAT_DONT_CARE")
    '("config-caveat-none" "GDK_GL_CONFIG_CAVEAT_NONE")
    '("slow-config" "GDK_GL_SLOW_CONFIG")
    '("non-conformant-config" "GDK_GL_NON_CONFORMANT_CONFIG")
  )
)

(define-enum VisualType
  (in-module "Gdk")
  (c-name "GdkGLVisualType")
  (gtype-id "GDK_TYPE_GL_VISUAL_TYPE")
  (values
    '("visual-type-dont-care" "GDK_GL_VISUAL_TYPE_DONT_CARE")
    '("true-color" "GDK_GL_TRUE_COLOR")
    '("direct-color" "GDK_GL_DIRECT_COLOR")
    '("pseudo-color" "GDK_GL_PSEUDO_COLOR")
    '("static-color" "GDK_GL_STATIC_COLOR")
    '("gray-scale" "GDK_GL_GRAY_SCALE")
    '("static-gray" "GDK_GL_STATIC_GRAY")
  )
)

(define-enum TransparentType
  (in-module "Gdk")
  (c-name "GdkGLTransparentType")
  (gtype-id "GDK_TYPE_GL_TRANSPARENT_TYPE")
  (values
    '("none" "GDK_GL_TRANSPARENT_NONE")
    '("rgb" "GDK_GL_TRANSPARENT_RGB")
    '("index" "GDK_GL_TRANSPARENT_INDEX")
  )
)

(define-flags DrawableTypeMask
  (in-module "Gdk")
  (c-name "GdkGLDrawableTypeMask")
  (gtype-id "GDK_TYPE_GL_DRAWABLE_TYPE_MASK")
  (values
    '("window-bit" "GDK_GL_WINDOW_BIT")
    '("pixmap-bit" "GDK_GL_PIXMAP_BIT")
    '("pbuffer-bit" "GDK_GL_PBUFFER_BIT")
  )
)

(define-flags RenderTypeMask
  (in-module "Gdk")
  (c-name "GdkGLRenderTypeMask")
  (gtype-id "GDK_TYPE_GL_RENDER_TYPE_MASK")
  (values
    '("rgba-bit" "GDK_GL_RGBA_BIT")
    '("color-index-bit" "GDK_GL_COLOR_INDEX_BIT")
  )
)

(define-flags BufferMask
  (in-module "Gdk")
  (c-name "GdkGLBufferMask")
  (gtype-id "GDK_TYPE_GL_BUFFER_MASK")
  (values
    '("front-left-buffer-bit" "GDK_GL_FRONT_LEFT_BUFFER_BIT")
    '("front-right-buffer-bit" "GDK_GL_FRONT_RIGHT_BUFFER_BIT")
    '("back-left-buffer-bit" "GDK_GL_BACK_LEFT_BUFFER_BIT")
    '("back-right-buffer-bit" "GDK_GL_BACK_RIGHT_BUFFER_BIT")
    '("aux-buffers-bit" "GDK_GL_AUX_BUFFERS_BIT")
    '("depth-buffer-bit" "GDK_GL_DEPTH_BUFFER_BIT")
    '("stencil-buffer-bit" "GDK_GL_STENCIL_BUFFER_BIT")
    '("accum-buffer-bit" "GDK_GL_ACCUM_BUFFER_BIT")
  )
)

(define-enum ConfigError
  (in-module "Gdk")
  (c-name "GdkGLConfigError")
  (gtype-id "GDK_TYPE_GL_CONFIG_ERROR")
  (values
    '("bad-screen" "GDK_GL_BAD_SCREEN")
    '("bad-attribute" "GDK_GL_BAD_ATTRIBUTE")
    '("no-extension" "GDK_GL_NO_EXTENSION")
    '("bad-visual" "GDK_GL_BAD_VISUAL")
    '("bad-context" "GDK_GL_BAD_CONTEXT")
    '("bad-value" "GDK_GL_BAD_VALUE")
    '("bad-enum" "GDK_GL_BAD_ENUM")
  )
)

(define-enum RenderType
  (in-module "Gdk")
  (c-name "GdkGLRenderType")
  (gtype-id "GDK_TYPE_GL_RENDER_TYPE")
  (values
    '("rgba-type" "GDK_GL_RGBA_TYPE")
    '("color-index-type" "GDK_GL_COLOR_INDEX_TYPE")
  )
)

(define-enum DrawableAttrib
  (in-module "Gdk")
  (c-name "GdkGLDrawableAttrib")
  (gtype-id "GDK_TYPE_GL_DRAWABLE_ATTRIB")
  (values
    '("preserved-contents" "GDK_GL_PRESERVED_CONTENTS")
    '("largest-pbuffer" "GDK_GL_LARGEST_PBUFFER")
    '("width" "GDK_GL_WIDTH")
    '("height" "GDK_GL_HEIGHT")
    '("event-mask" "GDK_GL_EVENT_MASK")
  )
)

(define-enum PbufferAttrib
  (in-module "Gdk")
  (c-name "GdkGLPbufferAttrib")
  (gtype-id "GDK_TYPE_GL_PBUFFER_ATTRIB")
  (values
    '("preserved-contents" "GDK_GL_PBUFFER_PRESERVED_CONTENTS")
    '("largest-pbuffer" "GDK_GL_PBUFFER_LARGEST_PBUFFER")
    '("height" "GDK_GL_PBUFFER_HEIGHT")
    '("width" "GDK_GL_PBUFFER_WIDTH")
  )
)

(define-flags EventMask
  (in-module "Gdk")
  (c-name "GdkGLEventMask")
  (gtype-id "GDK_TYPE_GL_EVENT_MASK")
  (values
    '("k" "GDK_GL_PBUFFER_CLOBBER_MASK")
  )
)

(define-enum EventType
  (in-module "Gdk")
  (c-name "GdkGLEventType")
  (gtype-id "GDK_TYPE_GL_EVENT_TYPE")
  (values
    '("damaged" "GDK_GL_DAMAGED")
    '("saved" "GDK_GL_SAVED")
  )
)

(define-enum DrawableType
  (in-module "Gdk")
  (c-name "GdkGLDrawableType")
  (gtype-id "GDK_TYPE_GL_DRAWABLE_TYPE")
  (values
    '("window" "GDK_GL_WINDOW")
    '("pbuffer" "GDK_GL_PBUFFER")
  )
)


;; From gtkglext/gdk/gdkgl.h



;; From gtkglext/gdk/gdkglconfig.h

(define-function gdk_gl_config_get_type
  (c-name "gdk_gl_config_get_type")
  (return-type "GType")
)

(define-function gdk_gl_config_new
  (c-name "gdk_gl_config_new")
  (is-constructor-of "GdkGLConfig")
  (return-type "GdkGLConfig*")
  (parameters
    '("const-int*" "attrib_list")
  )
)

(define-function gdk_gl_config_new_for_screen
  (c-name "gdk_gl_config_new_for_screen")
  (return-type "GdkGLConfig*")
  (parameters
    '("GdkScreen*" "screen")
    '("const-int*" "attrib_list")
  )
)

(define-function gdk_gl_config_new_by_mode
  (c-name "gdk_gl_config_new_by_mode")
  (return-type "GdkGLConfig*")
  (parameters
    '("GdkGLConfigMode" "mode")
  )
)

(define-function gdk_gl_config_new_by_mode_for_screen
  (c-name "gdk_gl_config_new_by_mode_for_screen")
  (return-type "GdkGLConfig*")
  (parameters
    '("GdkScreen*" "screen")
    '("GdkGLConfigMode" "mode")
  )
)

(define-method get_screen
  (of-object "GdkGLConfig")
  (c-name "gdk_gl_config_get_screen")
  (return-type "GdkScreen*")
)

(define-method get_attrib
  (of-object "GdkGLConfig")
  (c-name "gdk_gl_config_get_attrib")
  (return-type "gboolean")
  (parameters
    '("int" "attribute")
    '("int*" "value")
  )
)

(define-method get_colormap
  (of-object "GdkGLConfig")
  (c-name "gdk_gl_config_get_colormap")
  (return-type "GdkColormap*")
)

(define-method get_visual
  (of-object "GdkGLConfig")
  (c-name "gdk_gl_config_get_visual")
  (return-type "GdkVisual*")
)

(define-method get_depth
  (of-object "GdkGLConfig")
  (c-name "gdk_gl_config_get_depth")
  (return-type "gint")
)

(define-method get_layer_plane
  (of-object "GdkGLConfig")
  (c-name "gdk_gl_config_get_layer_plane")
  (return-type "gint")
)

(define-method get_n_aux_buffers
  (of-object "GdkGLConfig")
  (c-name "gdk_gl_config_get_n_aux_buffers")
  (return-type "gint")
)

(define-method get_n_sample_buffers
  (of-object "GdkGLConfig")
  (c-name "gdk_gl_config_get_n_sample_buffers")
  (return-type "gint")
)

(define-method is_rgba
  (of-object "GdkGLConfig")
  (c-name "gdk_gl_config_is_rgba")
  (return-type "gboolean")
)

(define-method is_double_buffered
  (of-object "GdkGLConfig")
  (c-name "gdk_gl_config_is_double_buffered")
  (return-type "gboolean")
)

(define-method is_stereo
  (of-object "GdkGLConfig")
  (c-name "gdk_gl_config_is_stereo")
  (return-type "gboolean")
)

(define-method has_alpha
  (of-object "GdkGLConfig")
  (c-name "gdk_gl_config_has_alpha")
  (return-type "gboolean")
)

(define-method has_depth_buffer
  (of-object "GdkGLConfig")
  (c-name "gdk_gl_config_has_depth_buffer")
  (return-type "gboolean")
)

(define-method has_stencil_buffer
  (of-object "GdkGLConfig")
  (c-name "gdk_gl_config_has_stencil_buffer")
  (return-type "gboolean")
)

(define-method has_accum_buffer
  (of-object "GdkGLConfig")
  (c-name "gdk_gl_config_has_accum_buffer")
  (return-type "gboolean")
)



;; From gtkglext/gdk/gdkglcontext.h

(define-function gdk_gl_context_get_type
  (c-name "gdk_gl_context_get_type")
  (return-type "GType")
)

(define-function gdk_gl_context_new
  (c-name "gdk_gl_context_new")
  (is-constructor-of "GdkGLContext")
  (return-type "GdkGLContext*")
  (parameters
    '("GdkGLDrawable*" "gldrawable")
    '("GdkGLContext*" "share_list")
    '("gboolean" "direct")
    '("int" "render_type")
  )
)

(define-method destroy
  (of-object "GdkGLContext")
  (c-name "gdk_gl_context_destroy")
  (return-type "none")
)

(define-method copy
  (of-object "GdkGLContext")
  (c-name "gdk_gl_context_copy")
  (return-type "gboolean")
  (parameters
    '("GdkGLContext*" "src")
    '("unsigned-long" "mask")
  )
)

(define-method get_gl_drawable
  (of-object "GdkGLContext")
  (c-name "gdk_gl_context_get_gl_drawable")
  (return-type "GdkGLDrawable*")
)

(define-method get_gl_config
  (of-object "GdkGLContext")
  (c-name "gdk_gl_context_get_gl_config")
  (return-type "GdkGLConfig*")
)

(define-method get_share_list
  (of-object "GdkGLContext")
  (c-name "gdk_gl_context_get_share_list")
  (return-type "GdkGLContext*")
)

(define-method is_direct
  (of-object "GdkGLContext")
  (c-name "gdk_gl_context_is_direct")
  (return-type "gboolean")
)

(define-method get_render_type
  (of-object "GdkGLContext")
  (c-name "gdk_gl_context_get_render_type")
  (return-type "int")
)

(define-function gdk_gl_context_get_current
  (c-name "gdk_gl_context_get_current")
  (return-type "GdkGLContext*")
)



;; From gtkglext/gdk/gdkgldebug.h



;; From gtkglext/gdk/gdkgldefs.h



;; From gtkglext/gdk/gdkgldrawable.h

(define-function gdk_gl_drawable_get_type
  (c-name "gdk_gl_drawable_get_type")
  (return-type "GType")
)

(define-method make_current
  (of-object "GdkGLDrawable")
  (c-name "gdk_gl_drawable_make_current")
  (return-type "gboolean")
  (parameters
    '("GdkGLContext*" "glcontext")
  )
)

(define-method is_double_buffered
  (of-object "GdkGLDrawable")
  (c-name "gdk_gl_drawable_is_double_buffered")
  (return-type "gboolean")
)

(define-method swap_buffers
  (of-object "GdkGLDrawable")
  (c-name "gdk_gl_drawable_swap_buffers")
  (return-type "none")
)

(define-method wait_gl
  (of-object "GdkGLDrawable")
  (c-name "gdk_gl_drawable_wait_gl")
  (return-type "none")
)

(define-method wait_gdk
  (of-object "GdkGLDrawable")
  (c-name "gdk_gl_drawable_wait_gdk")
  (return-type "none")
)

(define-method gl_begin
  (of-object "GdkGLDrawable")
  (c-name "gdk_gl_drawable_gl_begin")
  (return-type "gboolean")
  (parameters
    '("GdkGLContext*" "glcontext")
  )
)

(define-method gl_end
  (of-object "GdkGLDrawable")
  (c-name "gdk_gl_drawable_gl_end")
  (return-type "none")
)

(define-method get_gl_config
  (of-object "GdkGLDrawable")
  (c-name "gdk_gl_drawable_get_gl_config")
  (return-type "GdkGLConfig*")
)

(define-method get_size
  (of-object "GdkGLDrawable")
  (c-name "gdk_gl_drawable_get_size")
  (return-type "none")
  (parameters
    '("gint*" "width")
    '("gint*" "height")
  )
)

(define-function gdk_gl_drawable_get_current
  (c-name "gdk_gl_drawable_get_current")
  (return-type "GdkGLDrawable*")
)



;; From gtkglext/gdk/gdkglenumtypes.h

(define-function gdk_gl_config_caveat_get_type
  (c-name "gdk_gl_config_caveat_get_type")
  (return-type "GType")
)

(define-function gdk_gl_visual_type_get_type
  (c-name "gdk_gl_visual_type_get_type")
  (return-type "GType")
)

(define-function gdk_gl_transparent_type_get_type
  (c-name "gdk_gl_transparent_type_get_type")
  (return-type "GType")
)

(define-function gdk_gl_drawable_type_mask_get_type
  (c-name "gdk_gl_drawable_type_mask_get_type")
  (return-type "GType")
)

(define-function gdk_gl_render_type_mask_get_type
  (c-name "gdk_gl_render_type_mask_get_type")
  (return-type "GType")
)

(define-function gdk_gl_buffer_mask_get_type
  (c-name "gdk_gl_buffer_mask_get_type")
  (return-type "GType")
)

(define-function gdk_gl_config_error_get_type
  (c-name "gdk_gl_config_error_get_type")
  (return-type "GType")
)

(define-function gdk_gl_render_type_get_type
  (c-name "gdk_gl_render_type_get_type")
  (return-type "GType")
)

(define-function gdk_gl_drawable_attrib_get_type
  (c-name "gdk_gl_drawable_attrib_get_type")
  (return-type "GType")
)

(define-function gdk_gl_pbuffer_attrib_get_type
  (c-name "gdk_gl_pbuffer_attrib_get_type")
  (return-type "GType")
)

(define-function gdk_gl_event_mask_get_type
  (c-name "gdk_gl_event_mask_get_type")
  (return-type "GType")
)

(define-function gdk_gl_event_type_get_type
  (c-name "gdk_gl_event_type_get_type")
  (return-type "GType")
)

(define-function gdk_gl_drawable_type_get_type
  (c-name "gdk_gl_drawable_type_get_type")
  (return-type "GType")
)

(define-function gdk_gl_config_mode_get_type
  (c-name "gdk_gl_config_mode_get_type")
  (return-type "GType")
)



;; From gtkglext/gdk/gdkglext-config.h



;; From gtkglext/gdk/gdkglfont.h

(define-function gdk_gl_font_use_pango_font
  (c-name "gdk_gl_font_use_pango_font")
  (return-type "PangoFont*")
  (parameters
    '("const-PangoFontDescription*" "font_desc")
    '("int" "first")
    '("int" "count")
    '("int" "list_base")
  )
)

(define-function gdk_gl_font_use_pango_font_for_display
  (c-name "gdk_gl_font_use_pango_font_for_display")
  (return-type "PangoFont*")
  (parameters
    '("GdkDisplay*" "display")
    '("const-PangoFontDescription*" "font_desc")
    '("int" "first")
    '("int" "count")
    '("int" "list_base")
  )
)



;; From gtkglext/gdk/gdkglinit.h

(define-function gdk_gl_parse_args
  (c-name "gdk_gl_parse_args")
  (return-type "gboolean")
  (parameters
    '("int*" "argc")
    '("char***" "argv")
  )
)

(define-function gdk_gl_init_check
  (c-name "gdk_gl_init_check")
  (return-type "gboolean")
  (parameters
    '("int*" "argc")
    '("char***" "argv")
  )
)

(define-function gdk_gl_init
  (c-name "gdk_gl_init")
  (return-type "none")
  (parameters
    '("int*" "argc")
    '("char***" "argv")
  )
)



;; From gtkglext/gdk/gdkglpixmap.h

(define-function gdk_gl_pixmap_get_type
  (c-name "gdk_gl_pixmap_get_type")
  (return-type "GType")
)

(define-function gdk_gl_pixmap_new
  (c-name "gdk_gl_pixmap_new")
  (is-constructor-of "GdkGLPixmap")
  (return-type "GdkGLPixmap*")
  (parameters
    '("GdkGLConfig*" "glconfig")
    '("GdkPixmap*" "pixmap")
    '("const-int*" "attrib_list")
  )
)

(define-method destroy
  (of-object "GdkGLPixmap")
  (c-name "gdk_gl_pixmap_destroy")
  (return-type "none")
)

(define-method get_pixmap
  (of-object "GdkGLPixmap")
  (c-name "gdk_gl_pixmap_get_pixmap")
  (return-type "GdkPixmap*")
)

(define-method set_gl_capability
  (of-object "GdkPixmap")
  (c-name "gdk_pixmap_set_gl_capability")
  (return-type "GdkGLPixmap*")
  (parameters
    '("GdkGLConfig*" "glconfig")
    '("const-int*" "attrib_list")
  )
)

(define-method unset_gl_capability
  (of-object "GdkPixmap")
  (c-name "gdk_pixmap_unset_gl_capability")
  (return-type "none")
)

(define-method is_gl_capable
  (of-object "GdkPixmap")
  (c-name "gdk_pixmap_is_gl_capable")
  (return-type "gboolean")
)

(define-method get_gl_pixmap
  (of-object "GdkPixmap")
  (c-name "gdk_pixmap_get_gl_pixmap")
  (return-type "GdkGLPixmap*")
)

(define-method get_gl_drawable
  (of-object "GdkPixmap")
  (c-name "gdk_pixmap_get_gl_drawable")
  (return-type "GdkGLDrawable*")
)



;; From gtkglext/gdk/gdkglprivate.h



;; From gtkglext/gdk/gdkglquery.h

(define-function gdk_gl_query_extension
  (c-name "gdk_gl_query_extension")
  (return-type "gboolean")
)

(define-function gdk_gl_query_extension_for_display
  (c-name "gdk_gl_query_extension_for_display")
  (return-type "gboolean")
  (parameters
    '("GdkDisplay*" "display")
  )
)

(define-function gdk_gl_query_version
  (c-name "gdk_gl_query_version")
  (return-type "gboolean")
  (parameters
    '("int*" "major")
    '("int*" "minor")
  )
)

(define-function gdk_gl_query_version_for_display
  (c-name "gdk_gl_query_version_for_display")
  (return-type "gboolean")
  (parameters
    '("GdkDisplay*" "display")
    '("int*" "major")
    '("int*" "minor")
  )
)

(define-function gdk_gl_query_gl_extension
  (c-name "gdk_gl_query_gl_extension")
  (return-type "gboolean")
  (parameters
    '("const-char*" "extension")
  )
)

(define-function gdk_gl_get_proc_address
  (c-name "gdk_gl_get_proc_address")
  (return-type "GdkGLProc")
  (parameters
    '("const-char*" "proc_name")
  )
)



;; From gtkglext/gdk/gdkglshapes.h

(define-function gdk_gl_draw_cube
  (c-name "gdk_gl_draw_cube")
  (return-type "none")
  (parameters
    '("gboolean" "solid")
    '("double" "size")
  )
)

(define-function gdk_gl_draw_sphere
  (c-name "gdk_gl_draw_sphere")
  (return-type "none")
  (parameters
    '("gboolean" "solid")
    '("double" "radius")
    '("int" "slices")
    '("int" "stacks")
  )
)

(define-function gdk_gl_draw_cone
  (c-name "gdk_gl_draw_cone")
  (return-type "none")
  (parameters
    '("gboolean" "solid")
    '("double" "base")
    '("double" "height")
    '("int" "slices")
    '("int" "stacks")
  )
)

(define-function gdk_gl_draw_torus
  (c-name "gdk_gl_draw_torus")
  (return-type "none")
  (parameters
    '("gboolean" "solid")
    '("double" "inner_radius")
    '("double" "outer_radius")
    '("int" "nsides")
    '("int" "rings")
  )
)

(define-function gdk_gl_draw_tetrahedron
  (c-name "gdk_gl_draw_tetrahedron")
  (return-type "none")
  (parameters
    '("gboolean" "solid")
  )
)

(define-function gdk_gl_draw_octahedron
  (c-name "gdk_gl_draw_octahedron")
  (return-type "none")
  (parameters
    '("gboolean" "solid")
  )
)

(define-function gdk_gl_draw_dodecahedron
  (c-name "gdk_gl_draw_dodecahedron")
  (return-type "none")
  (parameters
    '("gboolean" "solid")
  )
)

(define-function gdk_gl_draw_icosahedron
  (c-name "gdk_gl_draw_icosahedron")
  (return-type "none")
  (parameters
    '("gboolean" "solid")
  )
)

(define-function gdk_gl_draw_teapot
  (c-name "gdk_gl_draw_teapot")
  (return-type "none")
  (parameters
    '("gboolean" "solid")
    '("double" "scale")
  )
)



;; From gtkglext/gdk/gdkgltokens.h



;; From gtkglext/gdk/gdkgltypes.h



;; From gtkglext/gdk/gdkglversion.h



;; From gtkglext/gdk/gdkglwindow.h

(define-function gdk_gl_window_get_type
  (c-name "gdk_gl_window_get_type")
  (return-type "GType")
)

(define-function gdk_gl_window_new
  (c-name "gdk_gl_window_new")
  (is-constructor-of "GdkGLWindow")
  (return-type "GdkGLWindow*")
  (parameters
    '("GdkGLConfig*" "glconfig")
    '("GdkWindow*" "window")
    '("const-int*" "attrib_list")
  )
)

(define-method destroy
  (of-object "GdkGLWindow")
  (c-name "gdk_gl_window_destroy")
  (return-type "none")
)

(define-method get_window
  (of-object "GdkGLWindow")
  (c-name "gdk_gl_window_get_window")
  (return-type "GdkWindow*")
)

(define-method set_gl_capability
  (of-object "GdkWindow")
  (c-name "gdk_window_set_gl_capability")
  (return-type "GdkGLWindow*")
  (parameters
    '("GdkGLConfig*" "glconfig")
    '("const-int*" "attrib_list")
  )
)

(define-method unset_gl_capability
  (of-object "GdkWindow")
  (c-name "gdk_window_unset_gl_capability")
  (return-type "none")
)

(define-method is_gl_capable
  (of-object "GdkWindow")
  (c-name "gdk_window_is_gl_capable")
  (return-type "gboolean")
)

(define-method get_gl_window
  (of-object "GdkWindow")
  (c-name "gdk_window_get_gl_window")
  (return-type "GdkGLWindow*")
)

(define-method get_gl_drawable
  (of-object "GdkWindow")
  (c-name "gdk_window_get_gl_drawable")
  (return-type "GdkGLDrawable*")
)


