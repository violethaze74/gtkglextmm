// -*- C++ -*-
/* gdkglextmm - C++ Wrapper for GdkGLExt
 * Copyright (C) 2002-2003  Naofumi Yasufuku
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.
 */

#include <gdkmm/gl/defs.h>

#ifdef GDKGLEXTMM_MULTIHEAD_SUPPORT
#include <gdkmm/screen.h>
#endif // GDKGLEXTMM_MULTIHEAD_SUPPORT
#include <gdkmm/visual.h>
#include <gdkmm/colormap.h>

_DEFS(gdkmm/gl,gdkglext)
_PINCLUDE(glibmm/private/object_p.h)

namespace Gdk
{
namespace GL
{

_WRAP_ENUM(ConfigMode, GdkGLConfigMode, s#^GL_##)

  /** OpenGL frame buffer configuration.
   *
   *
   */

class Config : public Glib::Object
{
  _CLASS_GOBJECT(Config, GdkGLConfig, GDK_GL_CONFIG, Glib::Object, GObject)

  _IGNORE(gdk_gl_config_get_screen)

protected:

  explicit Config(const int* attrib_list);
#ifdef GDKGLEXTMM_MULTIHEAD_SUPPORT
  Config(const Glib::RefPtr<const Gdk::Screen>& screen,
         const int* attrib_list);
#endif // GDKGLEXTMM_MULTIHEAD_SUPPORT
  explicit Config(ConfigMode mode);
#ifdef GDKGLEXTMM_MULTIHEAD_SUPPORT
  Config(const Glib::RefPtr<const Gdk::Screen>& screen,
         ConfigMode mode);
#endif // GDKGLEXTMM_MULTIHEAD_SUPPORT

public:

  /** Returns an OpenGL frame buffer configuration that match
   * the specified attributes.
   *
   * @param attrib_list  a list of attribute/value pairs. The last
   *                     attribute must be Gdk::GL::ATTRIB_LIST_NONE.
   * @return  the new Gdk::GL::Config if it is successful,
   *          NULL RefPtr otherwise.
   */
  static Glib::RefPtr<Config> create(const int* attrib_list);

#ifdef GDKGLEXTMM_MULTIHEAD_SUPPORT
  static Glib::RefPtr<Config> create(const Glib::RefPtr<const Gdk::Screen>& screen,
                                     const int* attrib_list);
#endif // GDKGLEXTMM_MULTIHEAD_SUPPORT

  /** Returns an OpenGL frame buffer configuration that match
   * the specified display mode.
   *
   * @param mode  display mode bit mask.
   * @return  the new Gdk::GL::Config if it is successful,
   *          NULL RefPtr otherwise.
   */
  static Glib::RefPtr<Config> create(ConfigMode mode);

#ifdef GDKGLEXTMM_MULTIHEAD_SUPPORT
  static Glib::RefPtr<Config> create(const Glib::RefPtr<const Gdk::Screen>& screen,
                                     ConfigMode mode);
#endif // GDKGLEXTMM_MULTIHEAD_SUPPORT

public:

#ifdef GDKGLEXTMM_MULTIHEAD_SUPPORT
  
  /** Get Gdk::Screen.
   * @return The Gdk::Screen.
   */
  Glib::RefPtr<Gdk::Screen> get_screen();
  
  /** Get Gdk::Screen.
   * @return The Gdk::Screen.
   */
  Glib::RefPtr<const Gdk::Screen> get_screen() const;

#endif // GDKGLEXTMM_MULTIHEAD_SUPPORT

  _WRAP_METHOD(bool get_attrib(int attribute, int& value) const, gdk_gl_config_get_attrib)

  _WRAP_METHOD(Glib::RefPtr<Gdk::Colormap> get_colormap(), gdk_gl_config_get_colormap, refreturn)
  _WRAP_METHOD(Glib::RefPtr<const Gdk::Colormap> get_colormap() const, gdk_gl_config_get_colormap, refreturn)

  _WRAP_METHOD(Glib::RefPtr<Gdk::Visual> get_visual(), gdk_gl_config_get_visual, refreturn)
  _WRAP_METHOD(Glib::RefPtr<const Gdk::Visual> get_visual() const, gdk_gl_config_get_visual, refreturn)

  _WRAP_METHOD(int get_depth() const, gdk_gl_config_get_depth)

  _WRAP_METHOD(int get_layer_plane() const, gdk_gl_config_get_layer_plane)

  _WRAP_METHOD(int get_n_aux_buffers() const, gdk_gl_config_get_n_aux_buffers)

  _WRAP_METHOD(int get_n_sample_buffers() const, gdk_gl_config_get_n_sample_buffers)

  _WRAP_METHOD(bool is_rgba() const, gdk_gl_config_is_rgba)

  _WRAP_METHOD(bool is_double_buffered() const, gdk_gl_config_is_double_buffered)

  _WRAP_METHOD(bool is_stereo() const, gdk_gl_config_is_stereo)

  _WRAP_METHOD(bool has_alpha() const, gdk_gl_config_has_alpha)

  _WRAP_METHOD(bool has_depth_buffer() const, gdk_gl_config_has_depth_buffer)

  _WRAP_METHOD(bool has_stencil_buffer() const, gdk_gl_config_has_stencil_buffer)

  _WRAP_METHOD(bool has_accum_buffer() const, gdk_gl_config_has_accum_buffer)

};

} // namespace GL
} // namespace Gdk
