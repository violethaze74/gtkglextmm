// Generated by gtkmmproc -- DO NOT MODIFY!

#include <gdkmm/gl/window.h>
#include <gdkmm/gl/private/window_p.h>

// -*- C++ -*-
/* gdkglextmm - C++ Wrapper for GdkGLExt
 * Copyright (C) 2002-2003  Naofumi Yasufuku
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.
 */

#include <gdk/gdkgldrawable.h>
#include <gdk/gdkglwindow.h>

namespace Gdk
{
  namespace GL
  {

    Window::Window(const Glib::RefPtr<const Config>& glconfig,
                   const Glib::RefPtr<const Gdk::Window>& window,
                   const int* attrib_list)
      : Gdk::Drawable(reinterpret_cast<GdkDrawable*>(
          gdk_gl_window_new(const_cast<GdkGLConfig*>(Glib::unwrap<Config>(glconfig)),
                            const_cast<GdkWindow*>(Glib::unwrap<Gdk::Window>(window)),
                            attrib_list)))
    {}

  } // namespace GL
} // namespace Gdk


namespace
{
} // anonymous namespace


namespace Glib
{

Glib::RefPtr<Gdk::GL::Window> wrap(GdkGLWindow* object, bool take_copy)
{
  return Glib::RefPtr<Gdk::GL::Window>( dynamic_cast<Gdk::GL::Window*> (Glib::wrap_auto ((GObject*)(object), take_copy)) );
  //We use dynamic_cast<> in case of multiple inheritance.
}

} /* namespace Glib */


namespace Gdk
{

namespace GL
{


/* The *_Class implementation: */

const Glib::Class& Window_Class::init()
{
  if(!gtype_) // create the GType if necessary
  {
    // Glib::Class has to know the class init function to clone custom types.
    class_init_func_ = &Window_Class::class_init_function;

    // This is actually just optimized away, apparently with no harm.
    // Make sure that the parent type has been created.
    //CppClassParent::CppObjectType::get_type();

    // Create the wrapper type, with the same class/instance size as the base type.
    register_derived_type(gdk_gl_window_get_type());

    // Add derived versions of interfaces, if the C type implements any interfaces:
  }

  return *this;
}

void Window_Class::class_init_function(void* g_class, void* class_data)
{
  BaseClassType *const klass = static_cast<BaseClassType*>(g_class);
  CppClassParent::class_init_function(klass, class_data);

}


Glib::ObjectBase* Window_Class::wrap_new(GObject* object)
{
  return new Window((GdkGLWindow*)object);
}


/* The implementation: */

GdkGLWindow* Window::gobj_copy()
{
  reference();
  return gobj();
}

Window::Window(const Glib::ConstructParams& construct_params)
:
  Gdk::Drawable(construct_params)
{}

Window::Window(GdkGLWindow* castitem)
:
  Gdk::Drawable((GdkDrawable*)(castitem))
{}

Window::~Window()
{}


Window::CppClassType Window::window_class_; // initialize static member

GType Window::get_type()
{
  return window_class_.init().get_type();
}

GType Window::get_base_type()
{
  return gdk_gl_window_get_type();
}


Glib::RefPtr<Window> Window::create(const Glib::RefPtr<const Config>& glconfig, const Glib::RefPtr<const Gdk::Window>& window, const int* attrib_list)
{
  return Glib::RefPtr<Window>( new Window(glconfig, window, attrib_list) );
}
Glib::RefPtr<Gdk::Window> Window::get_window()
{

  Glib::RefPtr<Gdk::Window> retvalue = Glib::wrap((GdkWindowObject*)(gdk_gl_window_get_window(gobj())));

  if(retvalue)
    retvalue->reference(); //The function does not do a ref for us.
  return retvalue;
}

Glib::RefPtr<const Gdk::Window> Window::get_window() const
{

  Glib::RefPtr<const Gdk::Window> retvalue = Glib::wrap((GdkWindowObject*)(gdk_gl_window_get_window(const_cast<GdkGLWindow*>(gobj()))));

  if(retvalue)
    retvalue->reference(); //The function does not do a ref for us.
  return retvalue;
}


} // namespace GL

} // namespace Gdk


